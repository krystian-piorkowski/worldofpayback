import XCTest
@testable import WorldOfPAYBACK

class ServiceErrorTests: XCTestCase {

    func test_ErrorMessage_InvalidResponse() {
        let error = ServiceError.invalidResponse
        let errorMessage = error.errorMessage()
        XCTAssertEqual(errorMessage, local("Error_InvalidResponse"))
    }
    
    func test_ErrorMessage_InvalidURL() {
        let error = ServiceError.invalidURL
        let errorMessage = error.errorMessage()
        XCTAssertEqual(errorMessage, local("Error_InvalidURL"))
    }
    
    func test_ErrorMessage_NoInternetConnection() {
        let error = ServiceError.noInternetConnection
        let errorMessage = error.errorMessage()
        XCTAssertEqual(errorMessage, local("Error_NoInternetConnection"))
    }
    
    func test_ErrorMessage_DecodingError() {
        let error = ServiceError.decodingError("Parsing failed")
        let errorMessage = error.errorMessage()
        XCTAssertEqual(errorMessage, local("Error_DecodingError"))
    }
    
    func test_ErrorMessage_Custom() {
        let error = ServiceError.custom("Custom error message")
        let errorMessage = error.errorMessage()
        XCTAssertEqual(errorMessage, "Custom error message")
    }
}
