import XCTest
@testable import WorldOfPAYBACK

class NetworkRequestComposerTests: XCTestCase {

    func test_CreateRequest_Success() {
        let sut = NetworkRequestComposer()
        let mockRequestType = MockRequestType()
        
        do {
            let request = try sut.createRequest(forType: mockRequestType)
            XCTAssertEqual(request.url?.absoluteString, "https://api.payback.com/transactions")
            XCTAssertEqual(request.httpMethod, "GET")
        } catch {
            XCTFail("Unexpected error: \(error)")
        }
    }
}

struct MockRequestType: RequestsParametersProviding {
    let domain: String = "https://api.payback.com/"
    let httpMethod: String = "GET"
    let path: String = "transactions"
    func isStatusCodeAcceptable(code: Int) -> Bool { return true }
}
