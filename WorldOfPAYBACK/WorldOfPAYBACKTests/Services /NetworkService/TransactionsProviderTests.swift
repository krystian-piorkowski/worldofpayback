import XCTest
import Combine
@testable import WorldOfPAYBACK

class TransactionsProviderTests: XCTestCase {
    
    func test_GetTransactions_Success() {
        let mockURLRequestPerformer = MockURLRequestPerformer()
        let mockNetworkRequestComposer = MockNetworkRequestComposer()
        let sut = TransactionsProvider(urlRequestPerformer: mockURLRequestPerformer, networkRequestComposer: mockNetworkRequestComposer)
        
        let mockTransactionResponse = JosnDataExample.defaultJSNO
        let mockHTTPResponse = HTTPURLResponse(url: URL(string: "mock_url")!, statusCode: 200, httpVersion: nil, headerFields: nil)!
        
        let expectation = XCTestExpectation(description: "Get transactions expectation")
        
        mockURLRequestPerformer.mockRequestResult = Just((mockTransactionResponse, mockHTTPResponse))
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
        
        var receivedTransactions: [Transaction]?
        let cancellable = sut.getTransaction()
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    XCTFail("Fetching transactions failed with error: \(error)")
                }
                expectation.fulfill()
            }, receiveValue: { transactions in
                receivedTransactions = transactions
            })
        
        wait(for: [expectation], timeout: 5.0)
        
        XCTAssertNotNil(receivedTransactions, "Received transactions should not be nil")
        XCTAssertEqual(receivedTransactions?.count, 1, "Received transactions count should be 1")
        
        guard let transaction = receivedTransactions?.first else {
            XCTFail("Optional value should not be nil")
            return
        }
        XCTAssertEqual(transaction.partnerDisplayName, "REWE Group", "Received partnerDisplayName should match")
        XCTAssertEqual(transaction.alias.reference, "795357452000810", "Received alias reference should match")
        XCTAssertEqual(transaction.category, 1, "Received category should match")
        XCTAssertEqual(transaction.transactionDetail.description, "Punkte sammeln", "Received transaction description should match")
        let dateFormatter = ISO8601DateFormatter()
        dateFormatter.formatOptions = [.withInternetDateTime]
        let expectedBookingDate = dateFormatter.date(from: "2022-07-24T10:59:05+0200")
        XCTAssertEqual(transaction.transactionDetail.bookingDate, expectedBookingDate, "Received booking date should match")
        XCTAssertEqual(transaction.transactionDetail.value.amount, 124, "Received value amount should match")
        XCTAssertEqual(transaction.transactionDetail.value.currency, "PBP", "Received currency should match")
        
        
        cancellable.cancel()
    }
    
    func test_GetTransactions_Failure() {
        let mockURLRequestPerformer = MockURLRequestPerformer()
        let mockNetworkRequestComposer = MockNetworkRequestComposer()
        let sut = TransactionsProvider(urlRequestPerformer: mockURLRequestPerformer, networkRequestComposer: mockNetworkRequestComposer)
        
        let expectation = XCTestExpectation(description: "Get transactions failure expectation")
        
        mockURLRequestPerformer.mockRequestResult = Fail(error: NSError(domain: "test", code: 123, userInfo: nil))
            .eraseToAnyPublisher()
        
        var receivedError: Error?
        let cancellable = sut.getTransaction()
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    XCTFail("Fetching transactions should have failed")
                case .failure(let error):
                    receivedError = error
                    expectation.fulfill()
                }
            }, receiveValue: { _ in
                XCTFail("Should not receive transactions")
            })
        
        wait(for: [expectation], timeout: 5.0)
        
        XCTAssertNotNil(receivedError, "Received error should not be nil")
        
        cancellable.cancel()
    }
}

class MockURLRequestPerformer: URLRequestPerforming {
    
    var mockRequestResult: AnyPublisher<(Data, HTTPURLResponse), Error> = Empty().eraseToAnyPublisher()
    
    func request(for urlRequest: URLRequest) -> AnyPublisher<(Data, HTTPURLResponse), Error> {
        return mockRequestResult
    }
}


class MockNetworkRequestComposer: NetworkRequestComposing {
    
    func createRequest(forType requestType: RequestsParametersProviding) throws -> URLRequest {
        guard let url = URL(string: "https://example.com") else {
            throw URLError(.badURL)
        }
        return URLRequest(url: url)
    }
}
