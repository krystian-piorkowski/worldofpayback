import XCTest
import Combine
@testable import WorldOfPAYBACK

class URLRequestPerformerTests: XCTestCase {
    
    func test_Request_Success() {
        setMockProtocol(success: true)
        
        let sessionConfiguration = URLSessionConfiguration.ephemeral
        sessionConfiguration.protocolClasses = [MockURLProtocol.self]
        let session = URLSession(configuration: sessionConfiguration)
        
        let sut = URLRequestPerformer(session: session)
        
        let expectedData = """
                {
                  "items" : [
                    {
                      "partnerDisplayName" : "REWE Group",
                      "alias" : {
                        "reference" : "795357452000810"
                      },
                      "category" : 1,
                      "transactionDetail" : {
                        "description" : "Punkte sammeln",
                        "bookingDate" : "2022-07-24T10:59:05+0200",
                        "value" : {
                          "amount" : 124,
                          "currency" : "PBP"
                        }
                      }
                    }
                ]
                }
                """.data(using: .utf8)!
        
        let url = URL(string: "some_url")!
        let request = URLRequest(url: url)
        
        let expectation = XCTestExpectation(description: "Request expectation")
        var receivedData: Data?
        var receivedResponse: HTTPURLResponse?
        
        let cancellable = sut.request(for: request)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    XCTFail("Request failed with error: \(error)")
                }
                expectation.fulfill()
            }, receiveValue: { value in
                receivedData = value.0
                receivedResponse = value.1
            })
        
        wait(for: [expectation], timeout: 5.0)
        
        XCTAssertNotNil(receivedData, "Received data should not be nil")
        XCTAssertEqual(receivedData, expectedData, "Received data should match the expected data")
        XCTAssertNotNil(receivedResponse, "Received response should not be nil")
        XCTAssertEqual(receivedResponse?.statusCode, 200, "Received response status code should be 200")
        
        cancellable.cancel()
    }
    
    func test_Request_Failure() {
            setMockProtocol(success: false)
            let sessionConfiguration = URLSessionConfiguration.ephemeral
            sessionConfiguration.protocolClasses = [MockURLProtocol.self]
            let session = URLSession(configuration: sessionConfiguration)
            
            let sut = URLRequestPerformer(session: session)
            
            let url = URL(string: "invalid_url")!
            let request = URLRequest(url: url)
            
            let expectation = XCTestExpectation(description: "Request expectation")
            var receivedError: Error?
            let cancellable = sut.request(for: request)
                .sink(receiveCompletion: { completion in
                    switch completion {
                    case .finished:
                        XCTFail("Request should have failed")
                    case .failure(let error):
                        receivedError = error
                        expectation.fulfill()
                    }
                }, receiveValue: { _ in
                    XCTFail("Request should not have received a value")
                })
            
            wait(for: [expectation], timeout: 5.0)
            
            XCTAssertNotNil(receivedError, "Received error should not be nil")
            
            cancellable.cancel()
        }
}




