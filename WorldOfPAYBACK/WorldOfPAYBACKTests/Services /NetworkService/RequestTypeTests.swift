import XCTest
@testable import WorldOfPAYBACK

class RequestTypeTests: XCTestCase {

    func test_GetTransactions_Domain() {
        let sut = RequestType.getTransactions
        XCTAssertEqual(sut.domain, "https://api.payback.com/transactions")
    }
    
    func test_GetTransactions_HTTPMethod() {
        let sut = RequestType.getTransactions
        XCTAssertEqual(sut.httpMethod, "GET")
    }
    
    func test_GetTransactions_Path() {
        let sut = RequestType.getTransactions
        XCTAssertEqual(sut.path, "transactions")
    }
    
    func test_GetTransactions_IsStatusCodeAcceptable() {
        let sut = RequestType.getTransactions
        XCTAssertTrue(sut.isStatusCodeAcceptable(code: 200))
        XCTAssertFalse(sut.isStatusCodeAcceptable(code: 404))
    }
}
