import XCTest
@testable import WorldOfPAYBACK

class TransactionDetailViewModelTests: XCTestCase {

    func test_InitWithTransaction() {
        let transaction = MockTransactionListRowViewModel()
        
        let viewModel = TransactionDetailViewModel(transaction: transaction)
        
        XCTAssertEqual(viewModel.partnerDisplayName, "Mock Partner")
        XCTAssertEqual(viewModel.description, "Mock Transaction")
    }
}
