import XCTest
import Combine
@testable import WorldOfPAYBACK

extension TransactionListState: Equatable{
    public static func == (lhs: TransactionListState, rhs: TransactionListState) -> Bool {
        switch (lhs, rhs) {
        case (.loadingData, .loadingData), (.presentingData, .presentingData):
            return true
        case let (.error(errorModel1), .error(errorModel2)):
            return errorModel1.title == errorModel2.title &&
            errorModel1.text == errorModel2.text &&
            errorModel1.actionButtonText == errorModel2.actionButtonText
        default:
            return false
        }
    }
}

extension TransactionListRowViewModel:  Equatable {
    public static func == (lhs: TransactionListRowViewModel, rhs: TransactionListRowViewModel) -> Bool {
        return lhs.bookingDate == rhs.bookingDate &&
        lhs.displayableDate == rhs.displayableDate &&
        lhs.partnerDisplayName == rhs.partnerDisplayName &&
        lhs.description == rhs.description &&
        lhs.amount == rhs.amount &&
        lhs.currency == rhs.currency &&
        lhs.amountAndCurrency == rhs.amountAndCurrency &&
        lhs.category == rhs.category
    }
}


class TransactionListViewModelTests: XCTestCase {
    
    var sut: TransactionListViewModel!
    var mockTransactionsCoordinator: MockTransactionsCoordinator!
    
    override func setUp() {
        super.setUp()
        mockTransactionsCoordinator = MockTransactionsCoordinator()
        sut = TransactionListViewModel(transactionsProvider: MockTransactionsProvider(result: .success([])), coordinator: mockTransactionsCoordinator)
    }
    
    override func tearDown() {
        sut = nil
        mockTransactionsCoordinator = nil
        super.tearDown()
    }
    
    func test_FetchTransactions_Success() {
        let expectation = XCTestExpectation(description: "Fetch transactions")
        let transactions = generateTransactions()
        sut = TransactionListViewModel(transactionsProvider: MockTransactionsProvider(result: .success(transactions)), coordinator: mockTransactionsCoordinator)
        sut.fetchTransactions()
        let expectedTransactions = transactions
            .map { TransactionListRowViewModel(transaction: $0) }
            .sorted { $0.bookingDate > $1.bookingDate }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            XCTAssertEqual(self.sut.viewState, .presentingData)
            XCTAssertEqual(self.sut.filteredTransactions as? [TransactionListRowViewModel] , expectedTransactions)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 3)
    }
    
    func test_Init_SetsStrings() {
        XCTAssertEqual(sut.totalAmountText, local("TransactionListView_TotalAmount"))
        XCTAssertEqual(sut.filterImageSystemName, "line.horizontal.3.decrease.circle")
    }
    
    func test_FetchTransactions_Success_CalculatesTotal() {
        let expectation = XCTestExpectation(description: "Fetch transactions")
        
        sut = TransactionListViewModel(transactionsProvider: MockTransactionsProvider(result: .success(generateTransactions())), coordinator: mockTransactionsCoordinator)
        
        sut.fetchTransactions()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            XCTAssertEqual(self.sut.totalAmount, "100 USD + 200 EUR")
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 3)
    }
    
    
    func test_FetchTransactions_Error() {
        let expectation = XCTestExpectation(description: "Fetch transactions")
        
        sut = TransactionListViewModel(transactionsProvider: MockTransactionsProvider(result: .failure(ServiceError.invalidResponse)), coordinator: mockTransactionsCoordinator)
        sut.fetchTransactions()
        let errorVM = ErrorViewModel(
            title: local("Error_Title"),
            text: ServiceError.invalidResponse.errorMessage(),
            actionButtonText: local("Error_RetryAction"),
            action: {})
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            XCTAssertEqual(self.sut.viewState, .error(errorModel: errorVM))
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 3)
    }
    
    func test_OpenTransactionDetail() {
        let transactionDetail = MockTransactionListRowViewModel()
        sut.open(transactionDetail: transactionDetail)
        XCTAssertTrue(mockTransactionsCoordinator.transactionDetailOpened)
    }
    
    func test_OpenFilters() {
        sut.openFilters()
        XCTAssertTrue(mockTransactionsCoordinator.filtersOpened)
    }
    
    func test_SetFilter() {
        let expectation = XCTestExpectation(description: "Fetch transactions")
        
        let filter = "1"
        sut = TransactionListViewModel(transactionsProvider: MockTransactionsProvider(result: .success(generateTransactions())), coordinator: mockTransactionsCoordinator)
        sut.fetchTransactions()
        sut.setFilter(filter)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            XCTAssertEqual(self.sut.filteredTransactions.count, 1)
            XCTAssertEqual(self.sut.filteredTransactions.first?.category, filter)
            XCTAssertEqual(self.sut.filteredByText, String(format: local("TransactionListView_Category"), filter))
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 3)
    }
    
    func test_SetNilFilter() {
        let expectation = XCTestExpectation(description: "Fetch transactions")
        sut = TransactionListViewModel(transactionsProvider: MockTransactionsProvider(result: .success(generateTransactions())), coordinator: mockTransactionsCoordinator)
        sut.fetchTransactions()
        sut.setFilter(nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            XCTAssertEqual(self.sut.filteredTransactions.count, 2)
            XCTAssertEqual(self.sut.filteredByText, local("TransactionListView_AllCategories"))
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 3)
    }
    
    
    private func generateTransactions() -> [Transaction] {
        let transaction1 = Transaction(
            partnerDisplayName: "Partner 1",
            alias: Transaction.Alias(reference: "Ref1"),
            category: 1,
            transactionDetail: Transaction.TransactionDetail(
                description: "Description 1",
                bookingDate: Date(),
                value: Transaction.TransactionDetail.Value(
                    amount: 100,
                    currency: "USD"
                )))
        let transaction2 = Transaction(
            partnerDisplayName: "Partner 2",
            alias: Transaction.Alias(reference: "Ref2"),
            category: 2,
            transactionDetail: Transaction.TransactionDetail(
                description: "Description 2",
                bookingDate: Date(),
                value: Transaction.TransactionDetail.Value(
                    amount: 200,
                    currency: "EUR"
                )))
        
        return [transaction1, transaction2]
    }
}

