import XCTest
@testable import WorldOfPAYBACK
class TransactionListRowViewModelTests: XCTestCase {
    
    func test_InitWithTransaction() {
        let transaction = Transaction(
            partnerDisplayName: "Partner",
            alias: Transaction.Alias(reference: "Ref"),
            category: 1,
            transactionDetail: Transaction.TransactionDetail(
                description: "Description",
                bookingDate: Date(),
                value: Transaction.TransactionDetail.Value(amount: 100, currency: "USD")
            )
        )
        
        let viewModel = TransactionListRowViewModel(transaction: transaction)
        
        XCTAssertEqual(viewModel.partnerDisplayName, "Partner")
        XCTAssertEqual(viewModel.description, "Description")
        XCTAssertEqual(viewModel.amount, 100)
        XCTAssertEqual(viewModel.currency, "USD")
        XCTAssertEqual(viewModel.amountAndCurrency, "100 USD")
        XCTAssertEqual(viewModel.category, "1")
    }
}
