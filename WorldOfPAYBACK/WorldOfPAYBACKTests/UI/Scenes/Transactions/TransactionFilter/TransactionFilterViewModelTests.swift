import XCTest
@testable import WorldOfPAYBACK

class TransactionFilterViewModelTests: XCTestCase {

    func test_IdExist() {
        let sut = TransactionFilterViewModel(filters: [], oldPickedFilter: nil) { _ in }
        XCTAssertNotNil(sut.id)
    }

    func test_SetsFilters() {
        let filters: Set<String> = ["Filter1", "Filter2", "Filter3"]
        let sut = TransactionFilterViewModel(filters: filters, oldPickedFilter: nil) { _ in }
        XCTAssertEqual(sut.filters, filters)
    }
    
    func test_PickedFilter() {
        var selectedFilter: String?
        let sut = TransactionFilterViewModel(filters: [], oldPickedFilter: nil) { filter in
            selectedFilter = filter
        }
        sut.pickedFilter("SelectedFilter")
        XCTAssertEqual(selectedFilter, "SelectedFilter")
    }
    
    func test_OldPickedFilter() {
        let oldPickedFilter = "OldPickedFilter"
        let sut = TransactionFilterViewModel(filters: [], oldPickedFilter: oldPickedFilter) { _ in }
        XCTAssertEqual(sut.oldPickedFilter, oldPickedFilter)
    }
    
    func test_NoFilterText() {
        let sut = TransactionFilterViewModel(filters: [], oldPickedFilter: nil) { _ in }
        XCTAssertEqual(sut.noFilterText, local("FilterView_NoFilter"))
    }
    
    func test_Title() {
        let sut = TransactionFilterViewModel(filters: [], oldPickedFilter: nil) { _ in }
        XCTAssertEqual(sut.title, local("FilterView_Title"))
    }
    
}
