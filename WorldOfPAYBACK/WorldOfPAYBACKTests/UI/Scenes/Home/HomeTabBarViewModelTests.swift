import XCTest
@testable import WorldOfPAYBACK

class HomeTabBarViewModelTests: XCTestCase {

    func test_TransactionTab() {
        let sut = HomeTabBarViewModel()
        let transactionTab = sut.transactionTab
        
        XCTAssertEqual(transactionTab.labelText, NSLocalizedString("HomeTab_Transaction", comment: ""))
        XCTAssertEqual(transactionTab.labelSystemImage, "dollarsign.circle")
    }

    func test_SettingsTab() {
        let sut = HomeTabBarViewModel()
        let settingsTab = sut.settingsTab
        
        XCTAssertEqual(settingsTab.labelText, NSLocalizedString("HomeTab_Settings", comment: ""))
        XCTAssertEqual(settingsTab.labelSystemImage, "gear")
    }
}
