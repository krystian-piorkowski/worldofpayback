import XCTest
import SwiftUI
@testable import WorldOfPAYBACK
class LanguageSettingsViewModelTests: XCTestCase {

    func test_Title() {
        let sut = LanguageSettingsViewModel(appReload: {})
        XCTAssertEqual(sut.title, local("LanguageSettingsView_Title"))
    }

    
    func test_AvailableLanguages() {
        let sut = LanguageSettingsViewModel(appReload: {})
        XCTAssertEqual(sut.availableLanguages, [.english, .polish])
    }
    
    func test_SelectedLanguage() {
        let sut = LanguageSettingsViewModel(appReload: {})
        XCTAssertEqual(sut.selectedLanguage, LocalizationSettings.shared.readSavedLanguage())
    }
    
    func test_TextFor_English() {
        let sut = LanguageSettingsViewModel(appReload: {})
        XCTAssertEqual(sut.textFor(langage: .english), local("LanguageSettingsView_English"))
    }
    
    func test_TextFor_Polish() {
        let sut = LanguageSettingsViewModel(appReload: {})
        XCTAssertEqual(sut.textFor(langage: .polish), local("LanguageSettingsView_Polish"))
    }
}
