import XCTest
@testable import WorldOfPAYBACK

class ErrorViewModelTests: XCTestCase {
    
    func test_ErrorViewModelProperties_CorrectValues() {
        let title = "Error"
        let text = "An error occurred."
        let actionButtonText = "Retry"
        var actionExecuted = false
        let action: () -> Void = { actionExecuted = true }
        
        let sut = ErrorViewModel(title: title, text: text, actionButtonText: actionButtonText, action: action)
        
        XCTAssertEqual(sut.title, title)
        XCTAssertEqual(sut.text, text)
        XCTAssertEqual(sut.actionButtonText, actionButtonText)
        
        sut.action()
        XCTAssertTrue(actionExecuted)
    }
    
    func test_Conformance_CorrectValues() {
        let title = "Error"
        let text = "An error occurred."
        let actionButtonText = "Retry"
        var actionExecuted = false
        let action: () -> Void = { actionExecuted = true }
        
        let sut: ErrorViewModelProviding = ErrorViewModel(title: title, text: text, actionButtonText: actionButtonText, action: action)
        
        XCTAssertEqual(sut.title, title)
        XCTAssertEqual(sut.text, text)
        XCTAssertEqual(sut.actionButtonText, actionButtonText)
        
        sut.action()
        XCTAssertTrue(actionExecuted)
    }
}
