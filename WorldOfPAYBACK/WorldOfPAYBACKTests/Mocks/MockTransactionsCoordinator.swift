@testable import WorldOfPAYBACK

class MockTransactionsCoordinator: TransactionsCoordinating {
    var transactionDetailOpened = false
    var filtersOpened = false
    var filtersClosed = false
    
    func open(transactionDetail: any TransactionListRowViewModelProviding) {
        transactionDetailOpened = true
    }
    
    func open(filters: Set<String>) {
        filtersOpened = true
    }
    
    func closeFilters() {
        filtersClosed = true
    }
}
