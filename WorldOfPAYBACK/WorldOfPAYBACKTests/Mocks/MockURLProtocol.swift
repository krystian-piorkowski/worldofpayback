import Foundation
import Combine

@testable import WorldOfPAYBACK

class MockURLProtocol: URLProtocol {
    static var requestHandler: ((URLRequest) throws -> (HTTPURLResponse, Data))?
    
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    override func stopLoading() { }
    
    override func startLoading() {
         guard let handler = MockURLProtocol.requestHandler else {
            return
        }
        
        do {
            let (response, data)  = try handler(request)
            client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
            client?.urlProtocol(self, didLoad: data)
            client?.urlProtocolDidFinishLoading(self)
        } catch  {
            client?.urlProtocol(self, didFailWithError: error)
        }
    }
}

func setMockProtocol(success: Bool) {
    MockURLProtocol.requestHandler = { request in
        if success {
            let exampleData = JosnDataExample.defaultJSNO
            let response = HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: "2.0", headerFields: nil)!
            return (response, exampleData)
        } else {
            let error = NSError(domain: "MockErrorDomain", code: 500, userInfo: [NSLocalizedDescriptionKey: "Mock error occurred"])
            throw error
        }
    }
}

struct JosnDataExample {
    static let defaultJSNO = """
        {
          "items" : [
            {
              "partnerDisplayName" : "REWE Group",
              "alias" : {
                "reference" : "795357452000810"
              },
              "category" : 1,
              "transactionDetail" : {
                "description" : "Punkte sammeln",
                "bookingDate" : "2022-07-24T10:59:05+0200",
                "value" : {
                  "amount" : 124,
                  "currency" : "PBP"
                }
              }
            }
        ]
        }
        """
            .data(using: .utf8)!
}
