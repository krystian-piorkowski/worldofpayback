@testable import WorldOfPAYBACK
import Foundation

class MockTransactionListRowViewModel: TransactionListRowViewModelProviding {
    init(category: String = "Mock Category") {
        self.category = category
    }
    
    let id: UUID = UUID()
    let bookingDate: Date = Date()
    let displayableDate: String = "May 1, 2024"
    let partnerDisplayName: String = "Mock Partner"
    let description: String = "Mock Transaction"
    let amount: Int = 100
    let currency: String = "USD"
    var amountAndCurrency: String {
        return "\(amount) \(currency)"
    }
    let category: String 
}
