@testable import WorldOfPAYBACK
import Combine

class MockTransactionsProvider: TransactionsProviding {
    enum Result {
        case success([Transaction])
        case failure(Error)
    }
    
    let result: Result
    
    init(result: Result) {
        self.result = result
    }
    
    func getTransaction() -> AnyPublisher<[Transaction], Error> {
        return Future { promise in
            switch self.result {
            case .success(let transactions):
                promise(.success(transactions))
            case .failure(let error):
                promise(.failure(error))
            }
        }.eraseToAnyPublisher()
    }
}
