import XCTest
@testable import WorldOfPAYBACK

class MockUserDefaults: UserDefaultsProtocol {
    var store: [String: Any] = [:]
    
    func string(forKey defaultName: String) -> String? {
        return store[defaultName] as? String
    }
    
    func set(_ value: Any?, forKey defaultName: String) {
        store[defaultName] = value
    }
}

class LocalizationSettingsTests: XCTestCase {
    
    var sut: LocalizationSettings!
    var mockUserDefaults: MockUserDefaults!
    
    override func setUp() {
        super.setUp()
        mockUserDefaults = MockUserDefaults()
        sut = LocalizationSettings.shared
        sut.swap(userDefaults: mockUserDefaults)
    }
    
    override func tearDown() {
        sut = nil
        mockUserDefaults = nil
        super.tearDown()
    }
    
    func test_saveLanguage_WithEnglishLanguage_SetsCorrectLanguage() {
        let expectedLanguage: Language = .english
        sut.saveLanguage(expectedLanguage)
        XCTAssertEqual(mockUserDefaults.store["WorldOfPAYBACK_pickedLanguage"] as? String, expectedLanguage.rawValue)
    }
    
    func test_saveLanguage_WithPolishLanguage_SetsCorrectLanguage() {
        let expectedLanguage: Language = .polish
        sut.saveLanguage(expectedLanguage)
        XCTAssertEqual(mockUserDefaults.store["WorldOfPAYBACK_pickedLanguage"] as? String, expectedLanguage.rawValue)
    }
    
    func test_dateFormatter_WithEnglishLanguage_ReturnsCorrectFormat() {
        sut.saveLanguage(.english)
        let dateFormatter = sut.dateFormatter()
        XCTAssertEqual(dateFormatter.dateFormat, "MMMM dd yyyy")
    }
    
    func test_dateFormatter_WithPolishLanguage_ReturnsCorrectFormat() {
        sut.saveLanguage(.polish)
        let dateFormatter = sut.dateFormatter()
        XCTAssertEqual(dateFormatter.dateFormat, "dd MMMM yyyy")
    }
}
