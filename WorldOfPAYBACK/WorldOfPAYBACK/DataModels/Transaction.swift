import Foundation

struct Transaction: Decodable {
    let partnerDisplayName: String
    let alias: Alias
    let category: Int
    let transactionDetail: TransactionDetail
    
    struct Alias: Decodable {
        let reference: String
    }
    
    struct TransactionDetail: Decodable {
        let description: String?
        let bookingDate: Date
        let value: Value
        
        struct Value: Decodable {
            let amount: Int
            let currency: String
        }
    }
}

struct TransactionResponse: Decodable {
    let items: [Transaction]
    
    static func decoderForDate() -> JSONDecoder {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return decoder
    }
}
