import SwiftUI

struct SelectionView: View {
    @State private var isSelected = false
    private let label: String
    
    init(label: String, isSelected: Bool = false) {
        self.label = label
        _isSelected = State(initialValue: isSelected)
    }
    
    
    var body: some View {
        HStack {
            Text(label)
                .padding(.leading, 20)
                .font(.title2)
            
            Spacer()
            
            Circle()
                .fill(isSelected ? Color.green : Color.clear)
                .frame(width: 20, height: 20)
                .overlay(
                    Circle()
                        .stroke(Color.gray, lineWidth: 2)
                )
                .padding(.trailing, 10)
        }
        .frame(height: 50)
        .contentShape(Rectangle())
        .overlay(
            RoundedRectangle(cornerRadius: 20)
                .stroke(Color.gray.opacity(0.5), lineWidth: 2)
        )
        .cornerRadius(20)
        .padding(5)
    }
}
