protocol ErrorViewModelProviding {
    var title: String { get }
    var text: String { get }
    var actionButtonText: String { get }
    var action: () -> Void { get }
}

struct ErrorViewModel: ErrorViewModelProviding {
    var title: String
    var text: String
    var actionButtonText: String
    var action: () -> Void
}
