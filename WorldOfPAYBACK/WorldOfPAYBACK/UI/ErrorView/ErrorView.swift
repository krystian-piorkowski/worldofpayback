import SwiftUI

struct ErrorView: View {
    let viewModel: ErrorViewModelProviding
    
    var body: some View {
        VStack {
            Text(viewModel.title)
                .font(.title)
                .padding()
            
            Text(viewModel.text)
                .font(.body)
                .padding()
            Button(action: {
                viewModel.action()
            }) {
                Text(viewModel.actionButtonText)
                    .font(.body)
                    .padding()
                    .cornerRadius(8)
            }
            .padding()
        }
        .frame(maxWidth: .infinity)
        .background(
                RoundedRectangle(cornerRadius: 16)
                    .fill(Color.red.opacity(0.5))
            )
        .overlay(
            RoundedRectangle(cornerRadius: 16)
                .stroke(Color.red, lineWidth: 2)
        )
        .padding()
    }
    
}
