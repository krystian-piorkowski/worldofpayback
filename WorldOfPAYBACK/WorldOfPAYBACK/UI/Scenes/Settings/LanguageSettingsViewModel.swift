import SwiftUI

protocol LanguageSettingsViewModelProviding: ObservableObject {
    var availableLanguages: [Language] { get }
    var selectedLanguage: Language { get }
    var title: String { get }
    var appReload: () -> Void { get }
    
    func textFor(langage: Language) -> String
    func pick(language: Language)
}

class LanguageSettingsViewModel: LanguageSettingsViewModelProviding {
    
    var title: String = local("LanguageSettingsView_Title")
    var appReload: () -> Void

    var availableLanguages: [Language] = [.english, .polish]
    @Published var selectedLanguage: Language = LocalizationSettings.shared.readSavedLanguage()
    
    init(appReload: @escaping () -> Void) {
        self.appReload = appReload
    }
    
    func pick(language: Language) {
        guard language != selectedLanguage else { return }
        selectedLanguage = language
        LocalizationSettings.shared.saveLanguage(language)
        appReload()
    }
    
    func textFor(langage: Language) -> String {
        switch langage {
        case .english:
            local("LanguageSettingsView_English")
        case .polish:
            local("LanguageSettingsView_Polish")

        }
    }
}
