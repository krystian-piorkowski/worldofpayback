import SwiftUI

struct LanguageSettingsView<T: LanguageSettingsViewModelProviding>: View {
    @ObservedObject private var viewModel: T
    
    init(viewModel: T) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        VStack {
            Text(viewModel.title)
                .font(.title)
                .padding([.top, .bottom], 25)
            
            ForEach(viewModel.availableLanguages, id: \.self) { language in
                SelectionView(label: viewModel.textFor(langage: language), isSelected: language == viewModel.selectedLanguage)
                    .id(UUID())
                    .onTapGesture {
                        viewModel.pick(language: language)
                    }
                    .padding()
            }
            Spacer()
        }
    }
}
