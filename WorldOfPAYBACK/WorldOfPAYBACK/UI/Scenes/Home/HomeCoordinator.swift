import Foundation
import SwiftUI

enum HomeTab {
    case transactions
    case settings
}
 
class HomeCoordinator: ObservableObject {
    @Published var tab = HomeTab.transactions
    @Published var transactionCoordinator: TransactionsCoordinator!
    
    
    private let dataProvider: DataProviding
    
    init(dataProvider: DataProviding) {
        self.dataProvider = dataProvider
        self.transactionCoordinator = .init(dataProvider: dataProvider, parentCoordinator: self)
    }
    
    func reloadView() {
        transactionCoordinator = .init(dataProvider: dataProvider, parentCoordinator: self)
    }
}
