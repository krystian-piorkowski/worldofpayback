import SwiftUI

struct HomeCoordinatorView: View {
    @ObservedObject var coordinator: HomeCoordinator
    @State private var appReloadID = UUID()
    
    private func reloadApp() {
        appReloadID = UUID()
        coordinator.reloadView()
    }
    
    var body: some View {
        TabView(selection: $coordinator.tab) {
            
            TransactionsCoordinatorView(coordinator: coordinator.transactionCoordinator)
                .tabItem { Label(local("HomeTab_Transaction"),
                                 systemImage: "dollarsign.circle")
                }
                .tag(HomeTab.transactions)
            
            LanguageSettingsView(viewModel: LanguageSettingsViewModel(appReload: reloadApp))
                .tabItem { Label(local("HomeTab_Settings"),
                                 systemImage: "gear")
                }
                .tag(HomeTab.settings)
        }
        .id(appReloadID)
    }
}

