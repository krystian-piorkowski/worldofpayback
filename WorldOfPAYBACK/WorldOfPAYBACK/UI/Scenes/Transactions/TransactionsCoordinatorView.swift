import Combine
import SwiftUI

struct TransactionsCoordinatorView: View {
    
    @ObservedObject var coordinator: TransactionsCoordinator
    
    var body: some View {
        NavigationView {
            TransactionListView(viewModel: coordinator.viewModel, filterModifier: SheetModifier(
                    item: $coordinator.filterViewModel,
                    content: { viewModel in
                TransactionFilterView(viewModel: viewModel)
            }))
                .navigate(using: $coordinator.detailViewModel) { viewModel in
                    TransactionDetailView(viewModel: viewModel)
                }
        }
    }
}

