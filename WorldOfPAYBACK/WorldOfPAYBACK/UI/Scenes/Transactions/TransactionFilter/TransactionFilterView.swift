import SwiftUI

struct TransactionFilterView: View {
    
    private let viewModel: any TransactionFilterViewModelProviding
    @State private var oldFilter: String?
    
    init(viewModel: any TransactionFilterViewModelProviding) {
        self.viewModel = viewModel
        _oldFilter = State(initialValue: viewModel.oldPickedFilter)
    }
    
    var body: some View {
        VStack {
            Text(viewModel.title)
                .font(.title)
                .padding([.top, .bottom], 25)
            
            SelectionView(label: viewModel.noFilterText, isSelected: oldFilter == nil)
                .onTapGesture {
                    viewModel.pickedFilter(nil)
                }
                .padding()
            
            ForEach(viewModel.filters.sorted(), id: \.self) { filter in
                SelectionView(label: filter, isSelected: filter == oldFilter)
                    .onTapGesture {
                        viewModel.pickedFilter(filter)
                    }
                    .padding()
            }
            
            Spacer()
        }
    }
}
