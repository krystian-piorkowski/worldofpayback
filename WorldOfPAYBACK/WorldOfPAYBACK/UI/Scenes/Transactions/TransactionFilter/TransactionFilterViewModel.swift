import Foundation

protocol TransactionFilterViewModelProviding: Identifiable {
    var id: String { get }
    var filters: Set<String> { get }
    var pickedFilter: (String?) -> Void { get }
    var oldPickedFilter: String? { get }
    var noFilterText: String { get }
    var title: String { get }
}

extension TransactionFilterViewModelProviding {
    var id: String {  
        return UUID().uuidString
    }
}

class TransactionFilterViewModel: TransactionFilterViewModelProviding {
    
    let filters: Set<String>
    let pickedFilter: (String?) -> Void
    
    let noFilterText = local("FilterView_NoFilter")
    let title = local("FilterView_Title")
    let oldPickedFilter: String?
    
    init(filters: Set<String>, oldPickedFilter: String?, pickedFilter: @escaping (String?) -> Void) {
        self.filters = filters
        self.pickedFilter = pickedFilter
        self.oldPickedFilter = oldPickedFilter
    }
}
