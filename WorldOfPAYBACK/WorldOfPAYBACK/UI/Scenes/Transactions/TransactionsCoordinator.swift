import Combine

protocol TransactionsCoordinating: ObservableObject {
    func open(transactionDetail: any TransactionListRowViewModelProviding)
    func open(filters: Set<String>)
    func closeFilters()
}

class TransactionsCoordinator : TransactionsCoordinating {
    
    @Published var viewModel: TransactionListViewModel!
    @Published var detailViewModel: TransactionDetailViewModelProviding?
    @Published var filterViewModel: TransactionFilterViewModel? 
    
    private unowned let parent: HomeCoordinator
    
    init(dataProvider: DataProviding, parentCoordinator: HomeCoordinator) {
        self.parent = parentCoordinator
        let viewModel = TransactionListViewModel(
            transactionsProvider: dataProvider.transactionProvider,
            coordinator: self)
        viewModel.fetchTransactions()
        self.viewModel = viewModel
        
    }
    
    func open(transactionDetail: any TransactionListRowViewModelProviding) {
        detailViewModel = TransactionDetailViewModel(transaction: transactionDetail)
    }
    
    func open(filters: Set<String>) {
        filterViewModel = TransactionFilterViewModel(
            filters: filters,
            oldPickedFilter: viewModel.pickedFilter,
            pickedFilter: { [weak self] pickedFilter in
            self?.viewModel.setFilter(pickedFilter)
            self?.closeFilters()
        })
    }
    
    func closeFilters() {
        filterViewModel = nil
    }
}
