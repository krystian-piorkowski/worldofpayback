import Foundation
import Combine

enum TransactionListState {
    case loadingData
    case presentingData
    case error(errorModel: ErrorViewModelProviding)
}

protocol TransactionListViewModelProviding: ObservableObject {
    var viewState: TransactionListState { get }
    var filteredTransactions: [any TransactionListRowViewModelProviding] { get }
    var filterImageSystemName: String { get }
    var filteredByText: String { get }
    var totalAmountText: String { get }
    var totalAmount: String { get }
    
    func fetchTransactions()
    func open(transactionDetail: any TransactionListRowViewModelProviding)
    func openFilters()
}

class TransactionListViewModel: TransactionListViewModelProviding {
    
    private unowned let coordinator: any TransactionsCoordinating
    private var transactions: [any TransactionListRowViewModelProviding] = []
    
    private var availableFilters = Set<String>()
    var pickedFilter: String?
    private let transactionsProvider: TransactionsProviding
    private var cancellables: Set<AnyCancellable> = []
    
    @Published var filteredTransactions: [any TransactionListRowViewModelProviding] = [] {
        didSet { calculateTotalAmount() }
    }
    @Published var totalAmount: String = "0"
    @Published var filteredByText: String = ""
    var totalAmountText: String = local("TransactionListView_TotalAmount")
    var filterImageSystemName: String = "line.horizontal.3.decrease.circle"
    
    @Published var viewState: TransactionListState = .loadingData
    
    init(transactionsProvider: TransactionsProviding, coordinator: any TransactionsCoordinating) {
        self.transactionsProvider = transactionsProvider
        self.coordinator = coordinator
    }
    
    func fetchTransactions() {
        viewState = .loadingData
        transactions = []
        transactionsProvider.getTransaction()
            .receive(on: DispatchQueue.main)
            .map{ transactions in
                transactions.map(TransactionListRowViewModel.init)
            }
            .sink(receiveCompletion: { [weak self] completion in
                switch completion {
                case .finished:
                    if self?.transactions.count == 0 {
                        self?.set(error: ServiceError.custom(local("Error_Unknown")))
                    }
                case .failure(let error):
                    self?.set(error: error)
                }
            }, receiveValue: { [weak self] transactions in
                self?.viewState = .presentingData
                self?.transactions = transactions.sorted { $0.bookingDate > $1.bookingDate }
                self?.extractFilters()
                self?.filter(by: self?.pickedFilter)
            })
            .store(in: &cancellables)
    }
    
    private func set(error: Error) {
        let descriptiveError = error as? DescriptiveError
        let errorText = (descriptiveError != nil) ? descriptiveError!.errorMessage() : local("Error_Unknown")
        viewState = .error(errorModel: ErrorViewModel(
            title:  local("Error_Title"),
            text: errorText,
            actionButtonText: local("Error_RetryAction"),
            action: { [weak self] in
                self?.fetchTransactions()
            }))
    }
    
    func open(transactionDetail: any TransactionListRowViewModelProviding) {
        coordinator.open(transactionDetail: transactionDetail)
    }
    
    func openFilters() {
        coordinator.open(filters: availableFilters)
    }
    
    func setFilter(_ filter: String?) {
        pickedFilter = filter
        self.filter(by: filter)
    }
    
    private func extractFilters() {
        availableFilters = Set(transactions.map { $0.category })
    }
    
    private func filter(by category: String?) {
        guard let category else {
            filteredTransactions = transactions
            filteredByText = local("TransactionListView_AllCategories")
            return
        }
        pickedFilter = category
        filteredByText = String(format: local("TransactionListView_Category"), category)
        
        filteredTransactions = transactions.filter{ $0.category == category }
    }
    
    private func calculateTotalAmount() {
        var currencyTotals = [String: Int]()
        
        for transaction in filteredTransactions {
            let currency = transaction.currency
            let amount = transaction.amount
            
            if let currentTotal = currencyTotals[currency] {
                currencyTotals[currency] = currentTotal + amount
            } else {
                currencyTotals[currency] = amount
            }
        }
        let sortedCurrencyTotals = currencyTotals.sorted { $0.value < $1.value }
        
        totalAmount = sortedCurrencyTotals.map { "\($0.value) \($0.key)" }.joined(separator: " + ")
    }
}
