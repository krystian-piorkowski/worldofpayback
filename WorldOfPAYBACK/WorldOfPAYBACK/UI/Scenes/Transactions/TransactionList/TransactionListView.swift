import SwiftUI

struct TransactionListView<FilterModifier: ViewModifier, TransactionListViewModelProvider: TransactionListViewModelProviding >: View {
    @ObservedObject var viewModel: TransactionListViewModelProvider
    let filterModifier: FilterModifier
    
    init(viewModel: TransactionListViewModelProvider, filterModifier: FilterModifier) {
        self.viewModel = viewModel
        self.filterModifier = filterModifier
    }
    
    var body: some View {
        VStack {
            switch viewModel.viewState {
            case .presentingData:
                presentDataView()
            case .loadingData:
                loadingView()
            case .error(let errorModel):
                VStack(alignment: .leading) {
                    ErrorView(viewModel: errorModel)
                    Spacer()
                }
                
            }
        }
    }
    
    @ViewBuilder
    private func loadingView() -> some View {
        ProgressView()
            .progressViewStyle(CircularProgressViewStyle())
            .scaleEffect(2.0)
    }
    
    @ViewBuilder
    private func presentDataView() -> some View {
        VStack {
            List(viewModel.filteredTransactions, id: \.id) { transaction in
                VStack(alignment: .leading, spacing: 15) {
                    HStack {
                        Text(transaction.displayableDate)
                            .font(.subheadline)
                            .foregroundColor(.secondary)
                        Spacer()
                        Text(transaction.description)
                            .font(.subheadline)
                            .foregroundColor(.secondary)
                    }
                    HStack {
                        Text(transaction.partnerDisplayName)
                            .font(.title2)
                            .fontWeight(.bold)
                        Spacer()
                        Text(transaction.amountAndCurrency)
                            .font(.title2)
                            .fontWeight(.bold)
                    }
                }
                .padding(2)
                .navigateOnTap { viewModel.open(transactionDetail: transaction) }
            }
            .listStyle(PlainListStyle())
            .refreshable {
                viewModel.fetchTransactions()
            }
            .navigationBarItems(
                trailing: Button(action: viewModel.openFilters) {
                    Image(systemName: viewModel.filterImageSystemName)
                        .font(.title)
                        .foregroundColor(.blue)
                }
                    .modifier(filterModifier)
            )
            .navigationTitle(viewModel.filteredByText)
            
            HStack {
                Text(viewModel.totalAmountText)
                    .font(.title)
                    .fontWeight(.bold)
                Spacer()
                Text(viewModel.totalAmount)
                    .font(.title)
                    .fontWeight(.bold)
            }
            .padding(15)
        }
    }
}
