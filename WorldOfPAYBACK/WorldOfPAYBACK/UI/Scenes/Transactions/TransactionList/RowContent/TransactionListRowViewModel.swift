import Foundation

protocol TransactionListRowViewModelProviding: AnyObject, Identifiable {
    var id: UUID { get }
    var bookingDate: Date { get }
    var displayableDate: String { get }
    var partnerDisplayName: String { get }
    var description: String { get }
    var amount: Int { get }
    var currency: String { get }
    var amountAndCurrency: String { get }
    var category: String { get }
}

class TransactionListRowViewModel: TransactionListRowViewModelProviding {
    
    let id = UUID()
    let bookingDate: Date
    let displayableDate: String
    let partnerDisplayName: String
    let description: String
    let amountAndCurrency: String
    let category: String
    var amount: Int
    var currency: String
    
    init(transaction: Transaction) {
        self.bookingDate = transaction.transactionDetail.bookingDate
        self.partnerDisplayName = transaction.partnerDisplayName
        self.description = transaction.transactionDetail.description ?? ""
        self.amountAndCurrency = "\(transaction.transactionDetail.value.amount) \(transaction.transactionDetail.value.currency)"
        self.category = String(transaction.category)
        self.amount = transaction.transactionDetail.value.amount
        self.currency = transaction.transactionDetail.value.currency
        
        let dateFormatter = LocalizationSettings.shared.dateFormatter()
        self.displayableDate = dateFormatter.string(from: transaction.transactionDetail.bookingDate)
    }
}

