protocol TransactionDetailViewModelProviding {
    var partnerDisplayName: String { get }
    var description: String { get }

}

class TransactionDetailViewModel: TransactionDetailViewModelProviding {
    let partnerDisplayName: String
    let description: String
    
    init(transaction: any TransactionListRowViewModelProviding) {
        self.partnerDisplayName = transaction.partnerDisplayName
        self.description = transaction.description
    }
}
