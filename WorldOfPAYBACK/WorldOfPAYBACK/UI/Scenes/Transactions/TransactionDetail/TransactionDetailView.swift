import SwiftUI

struct TransactionDetailView: View {
    private var viewModel: TransactionDetailViewModelProviding
    
    init(viewModel: any TransactionDetailViewModelProviding) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        VStack(spacing: 20) {
            Text(viewModel.partnerDisplayName)
                .font(.title)
            Text(viewModel.description)
                .font(.title2)
        }
    }
}
