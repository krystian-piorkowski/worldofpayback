import SwiftUI

extension View {

    func navigateOnTap(_ action: @escaping () -> Void) -> some View {
        let shouldNavigateBinding = Binding(
            get: { false },
            set: { newValue in
                if newValue {
                    action()
                }
            }
        )
        return NavigationLink(
            destination: EmptyView(),
            isActive: shouldNavigateBinding
        ) {
            self
        }
    }

    func navigate<Item, Destination: View>(
        using itemBinding: Binding<Item?>,
        to destination: @escaping (Item) -> Destination
    ) -> some View {
        let shouldNavigateBinding = Binding(
            get: { itemBinding.wrappedValue != nil },
            set: { shouldNavigate in
                if !shouldNavigate {
                    itemBinding.wrappedValue = nil
                }
            }
        )
        return navigate(when: shouldNavigateBinding) {
            itemBinding.wrappedValue.map(destination)
        }
    }

    func navigate<Destination: View>(
        when isActive: Binding<Bool>,
        to destination: @escaping () -> Destination
    ) -> some View {
        overlay(
            NavigationLink(
                destination: isActive.wrappedValue ? destination() : nil,
                isActive: isActive,
                label: { EmptyView() }
            )
        )
    }

}

extension NavigationLink {

    init<T: Identifiable, D: View>(
        using itemBinding: Binding<T?>,
        to destination: @escaping (T) -> D,
        label: @escaping () -> Label
    ) where Destination == D? {
        let shouldNavigateBinding = Binding(
            get: { itemBinding.wrappedValue != nil },
            set: { shouldNavigate in
                if !shouldNavigate {
                    itemBinding.wrappedValue = nil
                }
            }
        )
        self.init(
            destination: itemBinding.wrappedValue.map(destination),
            isActive: shouldNavigateBinding,
            label: label
        )
    }

}
