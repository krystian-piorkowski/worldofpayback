import Foundation

enum Language: String {
    case english = "en"
    case polish = "pl"
}

protocol UserDefaultsProtocol {
    func string(forKey defaultName: String) -> String?
    func set(_ value: Any?, forKey defaultName: String)
}

extension UserDefaults: UserDefaultsProtocol {}

class LocalizationSettings {
    static let shared = LocalizationSettings()
    private init() {}
    
    private let defaultLanguage: Language = .english
    private var userDefaults: UserDefaultsProtocol = UserDefaults.standard
    
    func swap(userDefaults: UserDefaultsProtocol) {
        self.userDefaults = userDefaults
    }
    
    private var pickedLanguage: Language {
        get {
            guard let languageString = userDefaults.string(forKey: "WorldOfPAYBACK_pickedLanguage") else {
                return defaultLanguage
            }
            return Language(rawValue: languageString) ?? defaultLanguage
        }
        set {
            userDefaults.set(newValue.rawValue, forKey: "WorldOfPAYBACK_pickedLanguage")
        }
    }
    
    func getPickedLanguage() -> Language {
        return pickedLanguage
    }
    
    func saveLanguage(_ language: Language) {
        userDefaults.set([language.rawValue], forKey: "AppleLanguages")
        pickedLanguage = language
    }
    
    func readSavedLanguage() -> Language {
        return pickedLanguage
    }
    
    func dateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        switch pickedLanguage {
        case .english:
            dateFormatter.dateFormat = "MMMM dd yyyy"
        case .polish:
            dateFormatter.dateFormat = "dd MMMM yyyy"
        }
        return dateFormatter
    }
}
