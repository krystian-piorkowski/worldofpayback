import Foundation

func local(_ key: String) -> String {
    return LocalizableProvider.shared.local(key)
}

class LocalizableProvider {
    private lazy var enDictionary: [String: Any]? = dictionary(for: .english)
    private lazy var plDictionary: [String: Any]? = dictionary(for: .polish)
    
    static let shared =  LocalizableProvider()
    private init() {}
    
    func local(_ key: String) -> String {
        guard
            let dict = dictionaryForCurrentLanguage(),
            let localizedString = dict[key] as? String else {
            return local(key)
        }
        return localizedString
    }
    
    private func dictionaryForCurrentLanguage() -> [String: Any]? {
        let currentLanguage = LocalizationSettings.shared.getPickedLanguage()
        switch currentLanguage {
        case .english:
            return enDictionary
        case .polish:
            return plDictionary
        }
    }
    
    private func dictionary(for language: Language) -> [String: Any]? {
        guard let path = Bundle.main.path(forResource: "Localizable", ofType: "strings", inDirectory: "\(language.rawValue).lproj"),
              let dictionary = NSDictionary(contentsOfFile: path) as? [String: Any] else {
            return nil
        }
        return dictionary
    }
}
