import Foundation

class ConfigProvider {
    static let sherd = ConfigProvider()
    private init() {}
    
    func getBaseURL() -> String? {
        return Bundle.main.object(forInfoDictionaryKey: "baseURL") as? String
    }
}
