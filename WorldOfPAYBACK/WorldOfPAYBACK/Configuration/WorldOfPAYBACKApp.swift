import SwiftUI

@main
struct WorldOfPAYBACKApp: App {
    
    @StateObject var coordinator = HomeCoordinator(
        dataProvider: DataProvider(
            transactionProvider: TransactionsProvider(
                urlRequestPerformer: FakeURLRequestPerformer(),
                networkRequestComposer: NetworkRequestComposer()
            )
        )
    )
    
    var body: some Scene {
        WindowGroup {
            HomeCoordinatorView(coordinator: coordinator)
        }
    }
}
