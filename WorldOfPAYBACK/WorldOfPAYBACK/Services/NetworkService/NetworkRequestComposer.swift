import Foundation

protocol NetworkRequestComposing {
    func createRequest(forType requestType: RequestsParametersProviding) throws -> URLRequest
}

struct NetworkRequestComposer: NetworkRequestComposing {
    func createRequest(forType requestType: RequestsParametersProviding) throws -> URLRequest {
        guard let url =  URL(string: "\(requestType.domain)\(requestType.path)") else {
            throw ServiceError.invalidURL
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = requestType.httpMethod
        request.cachePolicy = .useProtocolCachePolicy
        
        return request
    }
}

