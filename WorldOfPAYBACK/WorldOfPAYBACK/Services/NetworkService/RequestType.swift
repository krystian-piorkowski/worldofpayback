protocol RequestsParametersProviding {
    var domain: String { get }
    var httpMethod: String { get }
    var path: String { get }
    func isStatusCodeAcceptable(code: Int) -> Bool
}

enum RequestType: RequestsParametersProviding {
    case getTransactions
    
    var domain: String { ConfigProvider.sherd.getBaseURL() ?? "" }
    var httpMethod: String { "GET" }
    var path: String { "transactions" }
    
    func isStatusCodeAcceptable(code: Int) -> Bool {
        return code == 200
    }
}
