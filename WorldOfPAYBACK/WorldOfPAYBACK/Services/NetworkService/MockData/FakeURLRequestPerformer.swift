import Foundation
import Combine
import Network

struct FakeURLRequestPerformer: URLRequestPerforming {
    var session = URLSession.shared
    
    let testDataFileName = "PBTransactions"
    
    func request(for urlRequest: URLRequest) -> AnyPublisher<(Data, HTTPURLResponse), Error> {
        return Future<(Data, HTTPURLResponse), Error> { promise in
            
            let monitor = NWPathMonitor()
            let queue = DispatchQueue.global()
            monitor.start(queue: queue)
            
            monitor.pathUpdateHandler = { path in
                if path.status == .satisfied {
                    DispatchQueue.global().asyncAfter(deadline: .now() + 2) {
                        do {
                            guard let testDataURL = Bundle.main.url(forResource: testDataFileName, withExtension: "json"),
                                  let testData = try? Data(contentsOf: testDataURL) else {
                                throw ServiceError.custom(local("Error_MockDataLoad"))
                            }
                            guard Bool.random() else {
                                throw ServiceError.custom(local("Error_Random"))
                            }
                            
                            let urlResponse = HTTPURLResponse(url: urlRequest.url!, statusCode: 200, httpVersion: nil, headerFields: nil)!
                            promise(.success((testData, urlResponse)))
                        } catch {
                            promise(.failure(error))
                        }
                    }
                } else {
                    promise(.failure(ServiceError.noInternetConnection))
                }
            }
        }
        .receive(on: DispatchQueue.global())
        .eraseToAnyPublisher()
    }
    
    
}
