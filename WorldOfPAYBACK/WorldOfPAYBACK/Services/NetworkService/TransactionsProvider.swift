import Combine
import Foundation

protocol TransactionsProviding {
    func getTransaction() -> AnyPublisher <[Transaction], Error>
}

struct TransactionsProvider: TransactionsProviding {
    private let urlRequestPerformer: URLRequestPerforming
    private let networkRequestComposer: NetworkRequestComposing
    
    init(urlRequestPerformer: URLRequestPerforming, networkRequestComposer: NetworkRequestComposing) {
        self.urlRequestPerformer = urlRequestPerformer
        self.networkRequestComposer = networkRequestComposer
    }
    
    func getTransaction() -> AnyPublisher<[Transaction], Error> {
        do {
            let request = try networkRequestComposer.createRequest(forType: RequestType.getTransactions)
            return urlRequestPerformer.request(for: request)
                .tryMap { data, httpResponse in
                    guard RequestType.getTransactions.isStatusCodeAcceptable(code: httpResponse.statusCode) else {
                        throw ServiceError.invalidResponse
                    }
                    return data
                }
                .decode(type: TransactionResponse.self, decoder: TransactionResponse.decoderForDate())
                .map { $0.items }
                .eraseToAnyPublisher()
        } catch {
            return Fail(error: error).eraseToAnyPublisher()
        }
    }
}
