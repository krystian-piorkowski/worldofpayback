import Foundation
import Network
import Combine

protocol URLRequestPerforming {
    func request(for urlRequest: URLRequest) -> AnyPublisher<(Data, HTTPURLResponse), Error>
}

class URLRequestPerformer: URLRequestPerforming {
    
    private var session: URLSession
    private var cancellables: Set<AnyCancellable> = []

    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    

    func request(for urlRequest: URLRequest) -> AnyPublisher<(Data, HTTPURLResponse), any Error> {
        return Future<(Data, HTTPURLResponse), Error> { promise in
            let monitor = NWPathMonitor()
            let queue = DispatchQueue.global()
            monitor.start(queue: queue)
            
            monitor.pathUpdateHandler = { [weak self] path in
                if path.status == .satisfied {
                    
                    guard let self else {
                        promise(.failure(ServiceError.custom( local("Error_Unknown"))))
                        return }
                    
                    self.session.dataTaskPublisher(for: urlRequest)
                        .tryMap { data, response in
                            guard let httpResponse = response as? HTTPURLResponse else {
                                throw ServiceError.invalidResponse
                            }
                            return (data, httpResponse)
                        }
                        .sink(receiveCompletion: { completion in
                            if case let .failure(error) = completion {
                                promise(.failure(error))
                            }
                        }, receiveValue: { value in
                            promise(.success(value))
                        })
                        .store(in: &self.cancellables)
                } else {
                    promise(.failure(ServiceError.noInternetConnection))
                }
            }
        }
        .eraseToAnyPublisher()
    }
}

