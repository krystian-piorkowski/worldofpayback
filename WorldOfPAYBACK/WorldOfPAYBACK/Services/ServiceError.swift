import Foundation

protocol DescriptiveError: Error {
    func errorMessage() -> String
}

enum ServiceError: Equatable, DescriptiveError {
    case invalidResponse
    case invalidURL
    case noInternetConnection
    case decodingError(_ for: String)
    case custom(String)
    
    func errorMessage() -> String {
        switch self {
        case .custom(let message):
            return message
        case .invalidResponse:
            return local("Error_InvalidResponse")
        case .invalidURL:
            return local("Error_InvalidURL")
        case .noInternetConnection:
            return local("Error_NoInternetConnection")
        case .decodingError(_):
            return local("Error_DecodingError")
        }
    }
}
