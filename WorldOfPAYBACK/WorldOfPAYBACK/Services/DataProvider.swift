import Combine

protocol DataProviding {
    var transactionProvider: TransactionsProviding { get }
}

struct DataProvider: DataProviding {
    let transactionProvider: TransactionsProviding
}
