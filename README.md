This is a challenge app featuring fake data load to main list view and simple detail views. Additionally this app is setup to be ready for additional translation and it has separate configuration for PROD and QA targets.    
App itself was designed with:
* SwiftUI 
* MVVM
* Coordinators (based on idea how to do it for SwiftUI found in this article https://quickbirdstudios.com/blog/coordinator-pattern-in-swiftui/)
* Decoupling of objects with dependency injection   
* Fair percentage of unit tests coverage
